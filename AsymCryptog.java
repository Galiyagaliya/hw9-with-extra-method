package com.company;
import java.math.*;
import java.util.*;

/**
 * Created by galiyashakenova on 28.05.16.
 *
 */
public class AsymCryptog {


    private final int bitlength = 512;
    private BigInteger p;
    private BigInteger q;
    private BigInteger kd;
    private BigInteger ke;
    private BigInteger N;

    AsymCryptog(int p, int q, int Kd, int Ke) {
        this.p = BigInteger.valueOf(p);
        this.q = BigInteger.valueOf(q);
        kd = BigInteger.valueOf(Kd);
        ke = BigInteger.valueOf(Ke);
        this.N = BigInteger.valueOf(p * q);
    }

    AsymCryptog(int p, int q) {
        this.p = BigInteger.valueOf(p);
        this.q = BigInteger.valueOf(q);
        Random randy = new Random();
        ke = BigInteger.probablePrime(bitlength / 2, randy);
        BigInteger phi = BigInteger.valueOf((p-1)*(q-1));
        ke = BigInteger.valueOf(3);
        while (phi.gcd(ke).compareTo(BigInteger.ONE) > 0 && ke.compareTo(phi) < 0) {
            ke = ke.add(BigInteger.ONE);
        }
        kd = ke.modInverse(phi);
        this.N = BigInteger.valueOf(p * q);
    }

    public int encrypt(int m) {
        return BigInteger.valueOf(m).modPow(ke, N).intValue();
    }

    public int decrypt(int m) {
        return BigInteger.valueOf(m).modPow(kd, N).intValue();
    }

    // Encrypt message
    public int[] encrypt(byte[] message) {

        int[] bytes = new int[message.length];

        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = encrypt(message[i]);
        }
        return bytes;
    }

    // Decrypt message
    public byte[] decrypt(int[] message) {
        byte[] result = new byte[message.length];
        for (int i = 0; i < message.length; i++) {
            result[i] = (byte) decrypt(message[i]);

        }
        return result;
    }

}

